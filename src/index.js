require('dotenv').config();
const path = require('path');
const express = require('express');
const getFriends = require('./getFriends');
const testResponse = require('./testResponse');

const app = express();

app.route('/friends/:id').get(getFriends);
app.route('/test').get(testResponse);

app.use(express.static('dist'));

app.use('*', (req, res) => {
  res.sendFile(path.resolve(__dirname, '..', 'dist', 'index.html'));
});

app.listen(80, (err) => {
  if (err) {
    console.error(err);
  } else {
    console.log('app is listening at 80');
  }
});
