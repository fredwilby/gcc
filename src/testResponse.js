const fs = require('fs');
const path = require('path');

module.exports = (req, res) => {
  fs.readFile(
    path.resolve(__dirname, 'test-response.json'), 'utf8',
    (err, data) => {
      if (err) {
        res.status(500).json(err.message());
      }

      res.status(200).json(JSON.parse(data));
    },
  );
};
