const fetchFriends = require('./util/fetchFriends');
const friendsOfFriends = require('./util/friendsOfFriends');
const getUserInfo = require('./util/getUserInfo');

module.exports = (req, res) => {
  if (!req.params.id || typeof req.params.id !== 'string') {
    res.status(400).json({ message: 'must provide a username' });
  }

  let username = req.params.id;
  if (username.indexOf('@') === 0) {
    username = username.substr(1);
  }

  if (username.length === 0) {
    res.status(400).json({ message: 'invalid username provided' });
  }

  return fetchFriends(username)
    .then(([, friends]) => {
      const friendSet = new Set(friends);

      /* get number of friends that match */
      return friendsOfFriends(friends)
        .then(fof => fof.map(([id, theirFriends]) =>
          [id, theirFriends.filter(el => friendSet.has(el)).length]));
    })
    .then((friendsMatchList) => {
      /* sort by number matching & return top 5 */
      friendsMatchList.sort((a, b) => b[1] - a[1]);
      return getUserInfo(friendsMatchList.slice(0, 5));
    })
    .then((userInfo) => {
      res.status(200).json({
        friends: userInfo,
      });
    })
    .catch((err) => {
      const error = Array.isArray(err) ? err[0] : err.message;
      res.status(500).json(error);
      console.error(err);
    });
};
