const client = require('./twitter');

module.exports = (id, isUserId) => new Promise((res, rej) => {
  const params = isUserId ? { user_id: id } : { screen_name: id };
  client.get('friends/ids', params, (err, friends) => {
    if (err) {
      return rej(err);
    }

    return res([id, friends.ids]);
  });
});
