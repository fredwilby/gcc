const client = require('./twitter');

const fetchUserInfo = userIds =>
  new Promise((res, rej) => {
    client.get(
      'users/lookup',
      { user_id: userIds.join(',') },
      (err, results) => {
        if (err) {
          return rej(err);
        }

        return res(results);
      },
    );
  });

/* matches: [[userId, numMatches]...] */
/* fetches user details from api and returns an array [[details, numMatches]...]
*  in the same order.
*/
module.exports = matches =>
  fetchUserInfo(matches.map(el => el[0]))
    .then(results =>
      matches.map(([userId, numMatches]) => [results.find(val => val.id === userId), numMatches]));
