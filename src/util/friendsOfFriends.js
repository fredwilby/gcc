const fetchFriends = require('./fetchFriends');

module.exports = (userIds) => {
  /* fetch 14 friends (limited to 15 api requests / 15min) */
  const toFetch = userIds.slice(0, 14);

  return Promise.all(toFetch.map(el => fetchFriends(el, true)));
};
